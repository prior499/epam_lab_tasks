<?php
function php_dynamic_calculator($operator)
{
    $result = 0;
    $cntargs = func_num_args();
    for($i = 1; $i < $cntargs; $i++) {
        $arg = func_get_arg($i);
        if(is_numeric($arg)) {
            if($result == 0) {
                $result = $arg;
            } else {
                if($operator === '+') {
                    $result += $arg;
                } elseif ($operator === '*') {
                    $result *= $arg;
                } elseif ($operator === '-') {
                    $result -= $arg;
                } elseif ($operator === '/') {
                    $result /= $arg;
                } else {
                    return 0;
                }
            }
        }
    }
    return $result;
}
$operator = '+';
echo '|'.$result = php_dynamic_calculator($operator, 1, '23', 'xd'); // Result: 24.
$operator = '*';
echo '|'.$result = php_dynamic_calculator($operator, 2, 'hg', 2, 3); // Result: 12.
$operator = '-';
echo '|'.$result = php_dynamic_calculator($operator, '2', '5', '5'); // Result: -8.
$operator = '/';
echo '|'.$result = php_dynamic_calculator($operator, 1, 4, 'test'); // Result: 0.25.
$operator = 3;
echo '|'.$result = php_dynamic_calculator(0, 16, 4, 2); // Result: 0.
