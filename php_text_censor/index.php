<?php
$text = 'My credit cart number is 0000 0000 0000 0000';
echo $text = sanitaizer($text); // "My credit cart number is <removed>".
$text = 'Hi!Did you find my number 1234 5313 6323 1453';
echo $text = sanitaizer($text); // "Hi! Did you find my number <removed>".
$text = 'Hi!My phone number + 375 29 111 11 11';
echo $text = sanitaizer($text); // "Hi! My phone number +375 29 111 11 11".

function sanitaizer($text)
{
    $pattern = '/(\d{4}\W+\d{4}\W+\d{4}\W+\d{4}|\d{16}$)/';
    $replacement = htmlentities ('<removed>');
    $result = preg_replace($pattern, $replacement, $text);

    return $result;
}