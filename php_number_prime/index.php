<?php
function php_number_prime()
{
    $arr_prime = [];
    for($num = 1; $num <= 100; $num++) {
        for ($x = 2; $x <= $num; $x++) {
            if($num == $x) {
                $arr_prime[] = $num;
                continue;
            }
            if($num%$x == 0) {
                break;
            }
        }
    }
    return $arr_prime;
}