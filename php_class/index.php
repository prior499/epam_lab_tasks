<?php

interface HomoSapiens {

}

interface Human extends HomoSapiens {
    public function gender();
    public function name();
}

abstract class Men implements Human {
    public function gender()
    {
        echo self::class;
    }
}

class John extends Men {
    public function name()
    {
       echo self::class;
    }
}


$john = new John();
$john->gender(); // men
$hs = $john instanceof HomoSapiens; // true
$h = $john instanceof Human; // true


